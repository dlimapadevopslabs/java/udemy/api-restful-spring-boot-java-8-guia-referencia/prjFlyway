package com.kazale.prjflyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrjFlywayApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrjFlywayApplication.class, args);
	}
}
