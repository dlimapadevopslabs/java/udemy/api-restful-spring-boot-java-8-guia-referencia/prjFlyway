create table empresa (
id bigint(20) not null,
cnpj varchar(255) not null,
data_atualizacao datetime not null,
data_criacao datetime not null,
razao_social varchar(255) not null
) engine=innodb default charset=utf8;

alter table empresa add primary key(id);

alter table empresa modify id bigint(20) not null auto_increment;